import './Cart.scss'


export function Cart(props) {
  const { cartItems, handleRemoveClick } = props;

  const productsInCart = cartItems.map((item) => {
    return (
      <div key={item.id} className="card_in-cart">
        <div className="in-card-wrapper">
          <h3 className="product-name">{item.name}</h3>
          <img src={item.image} alt={item.name} />
          <p className="product-price">{item.price} грн</p>
        </div>

        <div className="close-card" onClick={() => handleRemoveClick(item)}>
          <div className="close-card_item">&times;</div>
        </div>
      </div>
    );
  });
  return (
    <div className="cart-page">
      <h2 className="cart-page-title">Cart</h2>
      {productsInCart.length === 0 ? (
        <div className="cart-page-none">Cart is empty</div>
      ) : (
        <div className="cart-items">{productsInCart}</div>
      )}
    </div>
  );
}
